# Parse arguments
if {$argc < 2} {
  puts "ERROR: Not enough arguments passed to build.tcl"
  print_help()
  return 1
} else {
  set project_name [lindex $argv 0]
  set mode [lindex $argv 1]
}

set revision [exec git rev-parse HEAD | cut -c1-8]

# Create variable to determine operating system
variable OS [lindex $tcl_platform(os) 0]

# Configure Python environment based on OS
if { $OS == "Windows" } {
  set python python
} else {
  # Assuming user is running Ubuntu 18.04 (where python is version 2.7)
  set python python3
}
unset ::env(PYTHONPATH)
unset ::env(PYTHONHOME)

# Pre-empt the Vivado build script from changing the working directory
set ::origin_dir_loc "../"

# Ensure build directory does not exist
if {[file exist ${project_name}]} {
    # check that it's a directoryt
    if {! [file isdirectory ${project_name}]} {
        puts "${project_name} exists, but it's a file"
        exit
    } else {
        # Remove the directory
        file delete -force -- ${project_name}
    }
}

# Generate the Vivado project from the project TCL
set project_tcl [file normalize ../${project_name}.tcl]
puts "STATUS: Generating Vivado project from ${project_tcl}" 
source ${project_tcl}

set xpr_path [file normalize ${project_name}/{$project_name}.xpr]
puts "STATUS: Project generated successfully at ${xpr_path}"

# Rebuild user ip_repo's index before adding any source files
update_ip_catalog -rebuild

# Determine what mode we want to execute
if {${mode} == "gui"} {
  # Just open the project in GUI mode
  puts "===================================================================="
  puts "         GUI mode selected, opening project with Vivado"
  puts "===================================================================="
  start_gui
} elseif {${mode} == "build"} {
# Build the project
  puts "===================================================================="
  puts "          Build mode selected, executing build pipeline"
  puts "===================================================================="
  
  # Set the ID and build time as toplevel generics
  set decimal_revision [expr 0x$revision]
  set build_time_seconds [clock seconds]
  set_property generic "C_COMMIT_HASH=32'd$decimal_revision C_BUILD_TIME=64'd$build_time_seconds" [current_fileset]
  set_property verilog_define ${project_name} [current_fileset]
  
  # Run the build
  launch_runs impl_1 -jobs 8 -verbose
  wait_on_run impl_1
  
  # Store Git hash in USR_ACCESS config register
  open_checkpoint [file normalize [glob ${project_name}/${project_name}.runs/impl_1/*_routed.dcp]]
  set_property BITSTREAM.CONFIG.USR_ACCESS 0X$revision [current_design]
  write_checkpoint -force [file normalize [glob ${project_name}/${project_name}.runs/impl_1/*_routed.dcp]]
  
  # Write bistream (+ sysdef for Zynq SoC designs)
  set bitfile [file normalize ${project_name}/${project_name}.runs/impl_1/${project_name}.bit] 
  set hwdef   [file normalize ${project_name}/${project_name}.runs/impl_1/${project_name}.hwdef] 
  set sysdef  [file normalize ${project_name}/${project_name}.runs/impl_1/${project_name}.sysdef] 
  write_bitstream $bitfile
  if {[string match xczu* [get_property PART [current_project]]]} { 
    write_sysdef -hwdef $hwdef -bitfile $bitfile $sysdef
  }
  puts "STATUS: Build successful!"
  
  # Export the hardware definition file or bitfile
  file mkdir hardware
  
  # Export HDF file for Zynq SoC designs 		
  if {[string match xczu* [get_property PART [current_project]]]} { 
	if {[catch {set sysdef_path [file normalize [glob ${project_name}/${project_name}.runs/impl_1/*.sysdef]]}]} {
    # .sysdef not found
	  puts "ERROR: No SYSDEF (HDF) output found at \"${project_name}/${project_name}.runs/impl_1/\", run must have failed"
      return 1
	# .sysdef found
	} else {
	  set hdf_path [file normalize hardware/${project_name}_${revision}.hdf]
	  file copy -force $sysdef_path $hdf_path
	  puts "STATUS: Exported hardware definition file (with bitstream) to $hdf_path"
	}  
  
  # Export BIT file for standard, non-SoC designs 
  } else { 
    if {[catch {set bitfile_path [file normalize [glob ${project_name}/${project_name}.runs/impl_1/*.bit]]}]} {
      # .bit not found
      puts "ERROR: No bitstream output found at \"${project_name}/${project_name}.runs/impl_1/\", run must have failed"
      return 1  
    # .bit found
    } else {
    set bitfile_out_path [file normalize hardware/${project_name}_${revision}.bit]
    set binfile_out_path [file normalize hardware/${project_name}_${revision}.bin]
	set flash_mcs_out_path [file normalize hardware/${project_name}_${revision}.mcs]
    set debug_probes_out_path [file normalize hardware/${project_name}_${revision}.ltx]
    
    # Generate bin file
    write_cfgmem  -format bin -size 16 -interface SPIx4 -loadbit "up 0x0 $bitfile_path" -file $binfile_out_path -force
    
    # Generate Flash programming file
    puts "STATUS: Generating Flash programming file" 
    write_cfgmem  -format mcs -size 16 -interface SPIx4 -loadbit "up 0x0 $bitfile_path" -file $flash_mcs_out_path -force
    
    # Copy bistream and debug probes file (if one exists)	  
    file copy -force $bitfile_path $bitfile_out_path
    #if {![catch {set debug_probes_path [file normalize ${project_name}/${project_name}.runs/impl_1/debug_nets.ltx]}]} {
    #    file copy -force $debug_probes_path $debug_probes_out_path
    #    puts "STATUS: Exported debug probes file to $debug_probes_out_path"
    #}	
    puts "STATUS: Exported bitstream file to $bitfile_out_path"
    }
  }
  
# Invalid mode 
} else {
  puts "ERROR: Invalid mode argument"
  print_help
  return 1
}