# Xilinx Build System
This repository contains basic scripts to recreate the xilinx build environment using the project TCL file

## How to run
1. Copy build.tcl into the root of your project directory
2. Make a folder called vivado and `cd` into that folder
3. Run the following command
```
vivado -mode batch -source ../build.tcl -notrace -tclargs $DESIGN_NAME build
```
where `$DESIGN_NAME` should be the same as the .tcl you saved your project as.

If you would just like to source the project and open the GUI then run:
```
vivado -mode batch -source ../build.tcl -notrace -tclargs $DESIGN_NAME gui
```